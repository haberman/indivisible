#!/usr/bin/env python3
# -*- coding:utf-8 -*-


from textwrap import TextWrapper
from time import sleep
from thermalprinter import ThermalPrinter
from thermalprinter.constants import BarCodePosition, BarCode, CodePage

# printer.barcode_height(80)
# printer.barcode_position(BarCodePosition.BELOW)
# printer.barcode_width(3)
# printer.barcode('012345678901', BarCode.EAN13)

# Styles
# printer.out('Bold', bold=True)
# printer.out('Double height', double_height=True)
# printer.out('Double width', double_width=True)
# printer.out('Inverse', inverse=True)
# printer.out('Rotate 90°', rotate=True, codepage=CodePage.ISO_8859_1)
# printer.out('Strike', strike=True)
# printer.out('Underline', underline=1)
# printer.out('Upside down', upside_down=True)


txt = '''
"Come listen all unto my song;",
"It is no silly fable;",
"'Tis all about the mighty cord",
"They call the Atlantic Cable.",
"",
"Twice did his bravest efforts fail.",
"And yet his mind was stable,",
"He wa'n't the man to break his heart",
"Because he broke his cable.",
"",
"And may we honor evermore",
"The manly, bold, and stable,",
"And tell our sons, to make them brave,",
"How Cyrus laid the cable."'''


if __name__ == '__main__':
    printer = ThermalPrinter('/dev/serial0', 9600, heat_time=120)
    printer.codepage(CodePage.ISO_8859_1)

    status = printer.status()
    print(status)

    while status['paper']:
        status = printer.status()
        printer.out('nik ta mère')
        sleep(.1)

    print('end')
    while not status['paper']:
        print('no mo paper')
        sleep(2)

    printer.out('that\'s a refill biatch' )

    # for line in txt.splitlines():
    #     # print(line)
    #     if line == '"",':
    #         printer.write(b'\n')
    #         continue

    #     # carriage_pos = 1
    #     for char in line:
    #         # if carriage_pos == 31 and char == ' ':
    #         #     printer.write(b'\n')

    #         # print('{} -> {}'.format(carriage_pos, char))
    #         printer.write(char.encode())
    #         # carriage_pos += 1

    #     printer.feed(1)

    # # for c in 'testing printer.out char by char and see if it works and wraps - a point after each char marks a .1s of sleep':
    # #     printer.write(c.encode())
    # #     sleep(.1)

    # printer.feed(2)
    printer.close()
    print('main')
