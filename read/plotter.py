import matplotlib.pyplot as plt


def plot_reads(subplot_count, reads):
    f, plots = plt.subplots(subplot_count, sharex=True, sharey=True)

    x = [i for i in range(0, len(reads))]
    for i in range(len(plots)):
        ax = plots[i]

        y = [r[i] for r in reads]
        ax.plot(x, y)

    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    plt.show()
