# -*- coding:utf-8 -*-


from random import randint
import spidev
import time

CYCLE_COUNT = 20
CYCLE_SLEEP = .75
STROBE_COUNT = 4


class MCP23S17:
    IOCON = 0x0A
    ICON_BANK = 0x80
    ICON_MIRROR = 0x40
    ICON_SEQOP = 0x20
    ICON_HAEN = 0x08
    ICON_ODR = 0x04
    ICON_INTPOL = 0x02

    IODIR = 0x00
    IODIR_READ = 1
    IODIR_WRITE = 0

    GPIOA = 0x12

    port_value = None

    def __init__(self, bus=0, device=0, addr=0):
        self.spi = spidev.SpiDev()
        self.spi.open(bus, device)
        self.spi.max_speed_hz = 10000000
        self.spi.mode = 0b00
        self.spi.xfer2([0])

        self.devid = 0x40
        self.addr = addr

        self.port_value = 0
        self.set_iocon(self.ICON_SEQOP | self.ICON_HAEN)

    def clear_port(self):
        addr1 = self.devid | self.addr << 1 | self.IODIR_WRITE
        addr2 = self.GPIOA
        self.port_value = 0b0000

        self.spi.xfer2(list([addr1, addr2, self.port_value]))

    def write_pin(self, pin, value):
        addr1 = self.devid | self.addr << 1 | self.IODIR_WRITE
        addr2 = self.GPIOA
        if value:
            self.port_value |= (1 << pin)
        else:
            self.port_value &= ~(1 << pin)

        self.spi.xfer2(list([addr1, addr2, self.port_value]))

    def write_gpio(self, value):
        addr1 = self.devid | self.addr << 1 | self.IODIR_WRITE
        addr2 = self.GPIOA
        self.port_value = value

        self.spi.xfer2(list([addr1, addr2, self.port_value]))

    def set_dir(self, value):
        addr1 = self.devid | self.addr << 1 | self.IODIR_WRITE
        addr2 = self.IODIR
        self.port_value = value

        self.spi.xfer2(list([addr1, addr2,  self.port_value]))

    def set_iocon(self, value):
        addr1 = self.devid | self.addr << 1 | self.IODIR_WRITE
        addr2 = self.IOCON
        self.port_value = value

        self.spi.xfer2(list([addr1, addr2,  self.port_value]))


mcp23s17 = MCP23S17(0, 0, 0)


def to_bin(binstr):
    bin_str = format(int(binstr, 2), '#0{}b'.format(len(binstr)+2))
    return int(bin_str, 2)


def random_bin(q):
    return to_bin(''.join([str(randint(0, 1)) for r in range(q)]))


def mcp_release(strobes=STROBE_COUNT):
    mcp23s17.write_gpio(to_bin('0'*strobes))
    mcp23s17.set_dir(to_bin('1'*strobes))


def strobe_random(cycles=CYCLE_COUNT, strobes=STROBE_COUNT, sleep=CYCLE_SLEEP):
    mcp23s17.set_dir(to_bin('0'*strobes))
    for c in range(cycles):
        mcp23s17.clear_port()
        time.sleep(sleep)
        mcp23s17.write_gpio(random_bin(strobes))
    mcp_release()


def strobe_linear(cycles=CYCLE_COUNT, strobes=STROBE_COUNT, sleep=CYCLE_SLEEP):
    mcp23s17.set_dir(to_bin('0'*strobes))

    i = 0
    for c in range(cycles):
        binlist = ['0'] * strobes
        if i >= 4:
            i = 0
        binlist[i] = '1'
        time.sleep(sleep)
        mcp23s17.write_gpio(to_bin(''.join(binlist)))

        i += 1
    mcp_release()
