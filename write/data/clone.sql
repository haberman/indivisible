-- SQLite
PRAGMA foreign_keys=off;

BEGIN TRANSACTION;

ALTER TABLE statuses RENAME TO _statuses_old;

CREATE TABLE IF NOT EXISTS statuses
                           (status_id INTEGER PRIMARY KEY,
                            status_author TEXT,
                            status_content TEXT,
                            status_hashtags TEXT,
                            status_info TEXT);

INSERT INTO statuses (status_id,status_author,status_content,status_hashtags,status_info)
  SELECT status_id,status_author,status_content,status_hashtags,status_info
  FROM _statuses_old;

DROP TABLE _statuses_old;
COMMIT;

PRAGMA foreign_keys=on;