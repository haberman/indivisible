# -*- coding:utf-8 -*-

import os
import re
import json
import random

from operator import itemgetter
from itertools import groupby

from poemade import parser, guess_form, distance, rhymes
from poemade.tokenizer import tokenize


BATCH_POEMS = 60
BATCH_FILE_SPREAD = 10
BATCH_STANZA = 20


def re_compiles(formats):
    replacements = [f.format(r'[a-m]', r'[a-m]') for f in formats]
    return re.compile(r'(' + r'|'.join(replacements) + r')')


TERCETS = ['x{}x', '{}x{}', 'xx{}', '{}xx', '{}{}x', 'x{}{}']
QUATRAINS = ['xx{}{}', '{}{}xx', 'x{}x{}', '{}x{}x', 'x{}{}x', '{}xx{}']
RE_RHY_COMMON = re_compiles(TERCETS + QUATRAINS)

res_listing = None


def combine_twitter(source, candidates, idx=0):
    '''When source is a twoliner, we'll compose a haiku like poem by finding lines on the statuses table.'''

    s_shape, s_verses = source, source['verses']
    del s_shape['verses']

    c_forms = [(i, json.loads(c[4])) for i, c in enumerate(candidates)]
    c_forms.sort(key=lambda cf: distance(s_shape['form']['scheme'], cf[1]['form']['scheme']))

    c_idx = c_forms[0][0]
    candidate = candidates[c_idx]

    return s_verses, candidate


def combine_gutenberg(source, candidates, idx=0):
    '''
    Combinatorial method to generate a text from a given source and a bag of gutenberg candidates. The approach is traditionnal:
    1- filtering bad texts (that step could alwyays need imporvements);
    2- sorting candidates by levensthein distance of some meta scheme (like rhyme notation);
    3- mixing things together either at a stanza level (when source is at least a tercet) or on a line basis when we find crossing rhymes.
    '''

    def contiguous_range(ints):
        ranges = []
        for gb in groupby(enumerate(ints), lambda x: x[0]-x[1]):
            group = map(itemgetter(1), gb[1])
            group = list(map(int, group))
            ranges.append(range(group[0], group[-1]))

        return ranges

    candidate, c_shape = candidates[idx]
    c_bookid, c_stzidx, c_verses = candidate

    s_shape, s_verses = source, source['verses']
    del s_shape['verses']
    s_tokens, c_tokens = tokenize('\n'.join(s_verses)), tokenize('\n'.join(c_verses))

    # source should contain the smallest extract
    ss, st, sv = s_shape, s_tokens, s_verses
    cs, ct, cv = c_shape, c_tokens, c_verses
    if len(c_tokens) > len(s_tokens):
        ss, st, sv = c_shape, c_tokens, c_verses
        cs, ct, cv = s_shape, s_tokens, s_verses
    s_shape, s_tokens, s_verses = ss, st, sv
    c_shape, c_tokens, c_verses = cs, ct, cv

    # sort candidates' stanzas by comparing their rhymes notations' distance
    c_stzs = []
    s_rhymes, c_rhymes = s_shape['rhyme']['scheme'].split(' '), c_shape['rhyme']['scheme'].split(' ')
    for s in range(len([sr for sr in s_rhymes if len(sr) >= 2])):
        s_rhyme = s_rhymes[s]
        for c in range(len([cr for cr in c_rhymes if len(cr) >= 2])):
            c_stzs.append((distance(s_rhyme, c_rhymes[c]), s, c))
    c_out = sorted(c_stzs, key=lambda x: x[0])[0]
    s_stzidx = c_out[1]

    s_range, c_range = stanza_range(s_stzidx, s_rhymes), stanza_range(c_out[2], c_rhymes)
    c_rhymes = c_shape['rhyme']['scheme'][c_range.start:c_range.stop]
    s_rhymes = s_shape['rhyme']['scheme'][s_range.start:s_range.stop]

    combine_done = False
    lines_count = len(s_range) + len(c_range)

    # find out if there'es rhyming line
    if len(c_range) <= 2:  # TODO -> @bug this generates a keyerror on next call
                           # s_shape, s_verses = source, source['verses'] KeyError: 'verses'
        print('\n________small candidate recurse')
        return combine_gutenberg(source, candidates, idx+1)
    elif len(c_range) >= 3:  # TODO -> @incomplete

        s_rhy_common = [m.span() for m in RE_RHY_COMMON.finditer(s_rhymes.lower())]
        if len(s_rhy_common) > 0:
            print('\n_________shrinking range')
            s_values = [s_rhymes[sr[0]:sr[1]].lower() for sr in s_rhy_common]
            c_rhy_common = [m.span() for m in RE_RHY_COMMON.finditer(c_rhymes.lower())]
            for cr in c_rhy_common:
                c_value = c_rhymes[cr[0]:cr[1]].lower()

                # shrinks ranges to their common rhyming pattern
                try:
                    combine_done = True
                    sp_idx = s_values.index(c_value)
                    c_range = range(cr[0], cr[1])
                    s_range = range(s_rhy_common[sp_idx][0], s_rhy_common[sp_idx][1])
                    break

                # Shrinks source and candidate to their respective schemes, which is kind of butchery
                except ValueError:
                    if lines_count <= 8:
                        continue

                    combine_done = True
                    s_range = range(c_rhy_common[0][0], c_rhy_common[0][1])
                    c_range = range(cr[0], cr[1])
                    break

    lines_count = len(s_range) + len(c_range)
    if not combine_done or (combine_done and lines_count > 8):
        print('\n________pursuing on rhymes')
        c_srhymes = []
        for c in c_range:
            c_srhymes.extend([(s, c) for s in s_range if rhymes(c_tokens[c][-1], s_tokens[s][-1])])

        if len(c_srhymes) > 0:
            print('got crossing rhymes, should check when they occured')
            print(c_srhymes)
        elif lines_count > 8:
            c_pron = c_shape['form']['scheme'].split(',')
            s_pron = s_shape['form']['scheme'].split(',')
            s_cprons = []
            for c in c_range:
                cp = c_pron[c]
                for s in s_range:
                    s_cprons.append((distance(cp, s_pron[s]), s, c))

            s_cprons = [(cp[1], cp[2]) for cp in sorted(s_cprons, key=lambda x: x[0])]

            s_cprons_seen = set()
            s_cprons_unique = {}
            for item in s_cprons:
                if item[0] not in s_cprons_seen:
                    s_cprons_seen.add(item[0])
                    s_cprons_unique[item[0]] = [item[1]]
                else:
                    s_cprons_unique[item[0]].append(item[1])

            combine_done = False
            s_cprons = s_cprons_unique
            s_cprons_range = {}
            for it in s_cprons.items():
                contigs = [cr for cr in contiguous_range(it[1]) if len(cr) >= 1]
                if len(contigs) == 0:
                    continue

                contigs.sort(key=lambda x: len(x), reverse=True)
                s_cprons_range[it[0]] = contigs[0]

            s_cprons = s_cprons_range
            if len(s_cprons) > 0:
                s_cpron = sorted([(k, v) for k, v in s_cprons.items()],
                                 key=lambda x: len(x[1]), reverse=True)[0]

                c_range = s_cpron[1]
                s_range = range(s_cpron[0], s_range.stop)
                combine_done = True

            if not combine_done:
                print('last attempt to do something, simple butchery for now')
                c_range = range(c_range.start, c_range.start+4)
                s_range = range(s_range.start, s_range.start+4)

    return c_bookid, c_stzidx, s_verses[s_range.start:s_range.stop], c_verses[c_range.start:c_range.stop]


def stanza_sort(source, candidates):
    '''Returns a list of dict sorting stanzas being the best candidates to answer a source poem.
    Args:
        '''
    res = []

    stz_scheme = source['stanza']['scheme']
    mtr_scheme = source['metric']['scheme']
    rhy_scheme = source['rhyme']['scheme']

    for c in candidates:
        book_id = int(re.sub(r'\D', '', c[0]))
        poem_idx, verses = c[1], c[2]

        c_tokens = tokenize('\n'.join(verses))
        c_form = guess_form(c_tokens)

        d = 0
        d += distance(stz_scheme, c_form['stanza']['scheme'])
        d += distance(mtr_scheme, c_form['metric']['scheme'])
        d += distance(rhy_scheme, c_form['rhyme']['scheme'])

        res.append((d, (book_id, poem_idx, verses), c_form))

    res.sort(key=lambda x: x[0])
    return res


def stanza_range(idx, sizes):
    start = 0 if idx == 0 else sum([len(s) for s in sizes[:idx]], 0)
    start += idx
    stop = start + len(sizes[idx])

    return range(start, stop)


def book_sample(book_id, book, n):
    ''' Returns a random sample of `n` unique indices for poems pssing the `parser.is_poem` test.'''
    poems = [p['verses'] for p in book]
    res = []
    attempts = 0
    while len(res) <= n and len(poems) > 1 and attempts < len(poems) * 2:
        attempts += 1
        rand_idx = random.randint(0, len(poems)-1)
        candidate = poems[rand_idx]

        if len(candidate) < 4:
            continue

        if parser.is_poem(candidate):
            res.append((book_id, rand_idx, candidate))

        del poems[rand_idx]

    return res


def poems_rand_load(db, poems_folder, poems_amount=BATCH_POEMS, files_spread=BATCH_FILE_SPREAD):
    '''
    Returns random poems from the gutenberg "poetrified" folder.
    Using the `session` table, also ensures that those poems have never been used before.

    Args:
        db     (sqlite.Db): database instance for handling sessions;
        poems_folder (str): where to lookup for `-poems.json` files;
        poems_amount (int): quantity of poems to grab;
        files_spread (int): minimum amount of different files to open for the lookup.
    '''
    assert os.path.isdir(poems_folder)

    global res_listing
    if not res_listing:
        res_listing = [f for f in os.listdir(poems_folder) if f.endswith('.json')]

    sess_pgids = db.fetch_cols('history', ['history_res_id'])
    used_ids = set([r[0] for r in sess_pgids]) if len(sess_pgids) > 0 else set()

    poems_by_files = poems_amount / files_spread
    poems = []

    while len(poems) < poems_amount:
        rand_id = random.choice(res_listing)
        if rand_id in used_ids:
            continue

        with open('{}/{}'.format(poems_folder, rand_id)) as fp:
            sample = book_sample(rand_id, json.load(fp), round(poems_by_files))
            poems.extend(sample)

    return poems
