import re
import unicodedata

from poemade.tokenizer import tokenize

CHUNK_RANGE_SZ = 50
CHUNK_LINE_SZ = 60

MERGE_RANGE = 4
MERGE_LINE = 2

SEEK_MAX = 30
VERSE_MAX = 66
# @see https://github.com/aparrish/gutenberg-poetry-corpus

# costly and very small chance to appear (caught in header or footer)
re_http = re.compile(r'((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*')
re_didaskalia = re.compile(r'^\s*_(.{5,})|_[\S]{0,1}$')


poetry_checks = {
    # isn't in title case
    'not_title_case': lambda line: not line.istitle(),
    # doesn't begin or end with a digit
    'no_url': lambda line: not re_http.search(line),
    # for poetry in plays, it appears to be a norm that didaskalia are enclosed within underscores
    'no_didaskalia': lambda line: not re_didaskalia.search(line),
    # isn't title case when considering only longer words
    'not_mostly_title_case': lambda line: not ' '.join([w for w in line.split() if len(w) >= 5]).istitle(),
    'not_mostly_upper': lambda line: sum([1 for ch in line if ch.isupper()]) / (len(line)+.001) < .5,
    # doesn't begin with a bracket (angle or square)
    'no_bracket': lambda line: not any([line.startswith(ch) for ch in '[<']),
    'not_number': lambda line: not re.search(r'^\d', line) and not re.search(r'\d.{0,1}$', line),
}

unicode_to_cp1252 = [(u'\u200b', '')]


def lexical_diversity(text):
    return len(set(text)) / len(text)


def to_cp1252(lines):
    for l in lines:
        try:
            (w.encode('cp1252') for w in l.split(' '))
            return lines
        except UnicodeEncodeError:
            rep = dict((r[0], r[1]) for r in unicode_to_cp1252)
            pattern = re.compile(r'|'.join(rep.keys()), re.UNICODE)
            if pattern.match(l):
                text = pattern.sub(lambda m: rep[m.group(0)], l)
                return text.splitlines()
            else:
                return None


# TODO @fix -> hang forever
def is_poem(verses, line_size=VERSE_MAX):
    poem_score = 1
    para_score = 1
    inc_score = 1/line_size

    for v in verses:
        line_checks = {k: f(v.strip()) for k, f in poetry_checks.items()}
        if all(line_checks):
            poem_score += inc_score
        elif len(v) > line_size:
            poem_score -= inc_score

        # v = v.strip()
        mostly_text = sum([1 for ch in v if ch.isalpha()]) / (len(v)+.001) > .5
        if not mostly_text:
            poem_score -= inc_score
            para_score += inc_score

    return poem_score > para_score
