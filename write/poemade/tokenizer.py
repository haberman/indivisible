# -*- coding:utf-8 -*-
'''
Simple tokenizer. Remove or replace unwanted characters, and parse to a list of lists of sentences and words
'''

import re
import unicodedata


def syllables_count(word):
    vowels = ['a', 'e', 'i', 'o', 'u']

    on_vowel = False
    in_diphthong = False
    minsyl = 0
    maxsyl = 0
    lastchar = None

    word = word.lower()
    for c in word:
        is_vowel = c in vowels

        if on_vowel == None:
            on_vowel = is_vowel

        # y is a special case
        if c == 'y':
            is_vowel = not on_vowel

        if is_vowel:
            if not on_vowel:
                # We weren't on a vowel before.
                # Seeing a new vowel bumps the syllable count.
                minsyl += 1
                maxsyl += 1
            elif on_vowel and not in_diphthong and c != lastchar:
                # We were already in a vowel.
                # Don't increment anything except the max count,
                # and only do that once per diphthong.
                in_diphthong = True
                maxsyl += 1

        on_vowel = is_vowel
        lastchar = c

    # Some special cases:
    if word[-1] == 'e':
        minsyl -= 1
    # if it ended with a consonant followed by y, count that as a syllable.
    if word[-1] == 'y' and not on_vowel:
        maxsyl += 1

    return minsyl


def accents_remove(string):
    '''
    Removes unicode accents from a string, downgrading to the base character
    '''

    nfkd = unicodedata.normalize('NFKD', string)
    return u''.join([c for c in nfkd if not unicodedata.combining(c)])


def tokenize(poem, line_only=False):

    tokens = []

    # Problematic characters to replace before regex
    replacements = {u'-': u' ', u'—': u' ', u'\'d': u'ed'}

    for original, replacement in replacements.items():
        replaced = poem.replace(original, replacement)
    replaced = accents_remove(replaced)

    # Keep apostrophes, discard other non-alphanumeric symbols
    cleaned = re.sub(r'[^0-9a-zA-Z\s\']', '', replaced)

    for line in cleaned.split('\n'):
        if line_only:
            tokens.append(line.strip())
        else:
            tokens.append([word for word in line.strip().split(' ')])

    return tokens
